package cadena;

public class ExOh {

	/**
	 * Using the Java language, have the function ExOh(str) take the str
	 * parameter being passed and return the string true if there is an equal
	 * number of x's and o's, otherwise return the string false. Only these two
	 * letters will be entered in the string, no punctuation or numbers. For
	 * example: if str is "xooxxxxooxo" then the output should return false
	 * because there are 6 x's and 5 o's.
	 */

	public ExOh() {
		String cadena = "xoxo"; // ahlo
		System.out.println(exOh(cadena));
	}

	String exOh(String str) {

		String isRight = "false";

		int qX = 0;
		int qO = 0;

		for (int i = 0; i < str.length(); i++) {
			String letra = Character.toString(str.charAt(i));
			if (letra.equalsIgnoreCase("x")) {
				qX++;
			}
			if (letra.equalsIgnoreCase("o")) {
				qO++;
			}
		}

		if (qX == qO) {
			isRight = "true";
		}

		return isRight;
	}

	public static void main(String[] args) {
		new ExOh();
	}

}

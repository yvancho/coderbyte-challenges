package cadena;

public class ContarLetras {

	public ContarLetras() {
		String cadena = "Today, is the greatest day ever!";
		System.out.println(letterCount(cadena));
	}

	String letterCount(String str) {

		String greatest = "";

		String[] arr = str.split(" ");

		for (String palabra : arr) {

			String palabraMayor = palabra;
			int qPalabra = 0;

			for (int i = 0; i < palabra.length() - 1; i++) {

				char letra = palabra.charAt(i);
				char letraMayor = palabra.charAt(0);
				int qLetra = 0;
				int temp = 0;

				for (int j = 0; j < palabra.length(); j++) {
					if (letra == palabra.charAt(j)) {
						qLetra++;
					}
					if (qLetra > temp) {
						letraMayor = palabra.charAt(j);

					}
					greatest = palabra;
					temp = qLetra;

				}

			}
		}

		return greatest;
	}

	public static void main(String[] args) {
		new ContarLetras();
	}

}

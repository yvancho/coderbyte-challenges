package cadena;

public class ConversorTiempo {

	public ConversorTiempo() {
		int tiempo = -1;
		System.out.println(timeConvert(tiempo));
	}

	String timeConvert(int num) {

		if (num <= 0){
			return "0:0";
		}
		String cad = "";
		int horas = num / 60;
		int minutos = num % 60;
		cad = horas + ":" + minutos;
		return cad;
	}

	public static void main(String[] args) {
		new ConversorTiempo();
	}

}

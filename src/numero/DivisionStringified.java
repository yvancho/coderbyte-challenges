package numero;

import java.text.NumberFormat;
import java.util.Locale;

public class DivisionStringified {

	/**
	 * Using the Java language, have the function DivisionStringified(num1,num2)
	 * take both parameters being passed, divide num1 by num2, and return the
	 * result as a string with properly formatted commas. If an answer is only 3
	 * digits long, return the number with no commas (ie. 2 / 3 should output
	 * "1"). For example: if num1 is 123456789 and num2 is 10000 the output
	 * should be "12,346".
	 */

	public DivisionStringified() {
		int num1 = 2;
		int num2 = 3;
		System.out.println(divisionStringified(num1, num2));
	}

	String divisionStringified(int num1, int num2) {

		String numFormat = "";
		Double numero = Double.valueOf(num1) / Double.valueOf(num2);

		numFormat = NumberFormat.getNumberInstance(Locale.getDefault()).format(
				numero);

		return numFormat;

	}

	public static void main(String[] args) {
		new DivisionStringified();
	}

}

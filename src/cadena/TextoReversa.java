package cadena;


public class TextoReversa {

	public TextoReversa() {
		String str = "hola como Estas";
		System.out.println(firstReverse(str));
	}

	String firstReverse(String str) {

		// code goes here
		/*
		 * Note: In Java the return type of a function and the parameter types
		 * being passed are defined, so this return call must match the return
		 * type of the function. You are free to modify the return type.
		 */
		String cad= "";
		for (int i = str.length()-1; i >=0; i--) {
			char a = str.charAt(i);
			cad+=a;
		}

		return cad;

	}

	public static void main(String[] args) {
		new TextoReversa();
	}

}

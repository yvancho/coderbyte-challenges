package cadena;


public class AbCheck {

	/**
	 * Using the Java language, have the function ABCheck(str) take the str
	 * parameter being passed and return the string true if the characters a and
	 * b are separated by exactly 3 places anywhere in the string at least once
	 * (ie. "lane borrowed" would result in true because there is exactly three
	 * characters between a and b). Otherwise return the string false.
	 */

	public AbCheck() {
		String cadena = "af23b"; // ahlo
		System.out.println(abCheck(cadena));
	}

	String abCheck(String str) {

		String isRight = "false";

		for (int i = 0; i < str.length(); i++) {
			if (Character.toString(str.charAt(i)).equalsIgnoreCase("a")
					&& i < str.length() - 4) {
				System.out.println(str.charAt(i));
				if (Character.toString(str.charAt(i + 4)).equalsIgnoreCase("b")) {
					isRight = "true";
				}
			}
		}

		return isRight;
	}

	public static void main(String[] args) {
		new AbCheck();
	}

}

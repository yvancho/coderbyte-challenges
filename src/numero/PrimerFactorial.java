package numero;

public class PrimerFactorial {

	public PrimerFactorial() {
		int num = 4;
		System.out.println(firstFactorial(num));
	}

	int firstFactorial(int num) {

		// code goes here
		/*
		 * Note: In Java the return type of a function and the parameter types
		 * being passed are defined, so this return call must match the return
		 * type of the function. You are free to modify the return type.
		 */
		
		for (int i = 1; i <= num; i++) {
			
		}

		return num;

	}

	public static void main(String[] args) {
		new PrimerFactorial();
	}

}

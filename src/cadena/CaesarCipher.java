package cadena;

public class CaesarCipher {

	/**
	 * Using the Java language, have the function CaesarCipher(str,num) take the
	 * str parameter and perform a Caesar Cipher shift on it using the num
	 * parameter as the shifting number. A Caesar Cipher works by shifting each
	 * letter in the string N places down in the alphabet (in this case N will
	 * be num). Punctuation, spaces, and capitalization should remain intact.
	 * For example if the string is "Caesar Cipher" and num is 2 the output
	 * should be "Ecguct Ekrjgt".
	 */

	public CaesarCipher() {
		String cadena = "Caesar Cipher";
		System.out.println(caesarCipher(cadena, 2));
	}

	String caesarCipher(String str, int num) {
		String cad = "";
		for (int i = 0; i < str.length(); i++) {
			char letra = str.charAt(i);
			if (!Character.isLetter(letra)) {
				cad += (char) ((int) letra);
				continue;
			}
			cad += (char) ((int) letra + num);
		}
		return cad;
	}

	public static void main(String[] args) {
		new CaesarCipher();
	}

}

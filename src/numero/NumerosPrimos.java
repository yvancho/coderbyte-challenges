package numero;

public class NumerosPrimos {

	public NumerosPrimos() {
		int num = 2;
		System.out.println(primeTime(num));
	}

	String primeTime(int num) {

		String isPrime = "false";
		int q = 0;

		for (int i = 1; i <= num; i++) {
			if (num % i == 0) {
				q++;
			}
		}

		if (q == 2) {
			isPrime = "true";
		}

		return isPrime;

	}

	public static void main(String[] args) {
		new NumerosPrimos();
	}

}

package numero;

import java.util.Arrays;

public class ArreglosCrud {

	public ArreglosCrud() {
		int[] arr = { 5, 8, 13, 1, 6, 8 };

		System.out.println("Agregar ultimo: " + this.agregarItem(arr, 3));
		System.out.println("Agregar item at: " + this.agregarItemAt(arr, 2, 2));
		System.out.println("Remove at index: " + this.removerItem(arr, 4));
	}

	private String agregarItem(int[] arr, int num) {
		arr = Arrays.copyOf(arr, arr.length + 1);
		arr[arr.length - 1] = num;
		return concatenar(arr);
	}

	private String agregarItemAt(int[] arr, int ind, int num) {
		arr = Arrays.copyOf(arr, arr.length + 1);
		for (int i = arr.length - 1; i > ind; i--) {
			arr[i] = arr[i - 1];
		}
		arr[ind] = num;
		return concatenar(arr);
	}

	private String removerItem(int[] arr, int indice) {
		for (int i = indice; i < arr.length - 1; i++) {
			arr[i] = arr[i + 1];
		}
		arr = Arrays.copyOf(arr, arr.length - 1);
		return concatenar(arr);
	}

	public static String concatenar(int[] arr) {
		String cad = "";
		for (int i = 0; i < arr.length; i++) {
			cad += arr[i] + " ";
		}
		return cad;
	}

	public static void main(String[] args) {
		new ArreglosCrud();
	}

}

package numero;

public class ArithGeo {

	/**
	 * Using the Java language, have the function ArithGeo(arr) take the array
	 * of numbers stored in arr and return the string "Arithmetic" if the
	 * sequence follows an arithmetic pattern or return "Geometric" if it
	 * follows a geometric pattern. If the sequence doesn't follow either
	 * pattern return -1. An arithmetic sequence is one where the difference
	 * between each of the numbers is consistent, where as in a geometric
	 * sequence, each term after the first is multiplied by some constant or
	 * common ratio. Arithmetic example: [2, 4, 6, 8] and Geometric example: [2,
	 * 6, 18, 54]. Negative numbers may be entered as parameters, 0 will not be
	 * entered, and no array will contain all the same elements.
	 */

	public ArithGeo() {
		int[] arr1 = { 5, 10, 15 };
		int[] arr2 = { 2, 6, 18, 54 };
		System.out.println(arithGeo(arr1));
	}

	String arithGeo(int[] arr) {

		String retorno = "-1";

		int kAri = arr[1] - arr[0];
		int kGeo = arr[1] / arr[0];

		// Arithmetic
		boolean a = this.isArithmetic(arr, kAri);

		// Geometric
		boolean g = this.isGeometric(arr, kGeo);

		if (a) {
			retorno = "Arithmetic";
		} else if (g) {
			retorno = "Geometric";
		}

		return retorno;

	}

	private boolean isArithmetic(int[] arr, int kAri) {

		boolean b = false;

		for (int i = 0; i < arr.length - 1; i++) {
			int num = arr[i] + kAri;
			if (num == arr[i + 1]) {
				b = true;
			} else {
				b = false;
				break;
			}
		}

		return b;

	}

	private boolean isGeometric(int[] arr, int kGeo) {

		boolean b = false;

		for (int i = 0; i < arr.length - 1; i++) {
			int num = arr[i] * kGeo;
			if (num == arr[i + 1]) {
				b = true;
			} else {
				b = false;
				break;
			}
		}

		return b;

	}

	public static void main(String[] args) {
		new ArithGeo();
	}

}

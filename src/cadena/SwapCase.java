package cadena;

public class SwapCase {

	public SwapCase() {
		String str = "Hello World"; // hELLO wORLD
		System.out.println(letraCapital(str));
	}

	/**
	 * Using the Java language, have the function SwapCase(str) take the str
	 * parameter and swap the case of each character. For example: if str is
	 * "Hello World" the output should be hELLO wORLD. Let numbers and symbols
	 * stay the way they are.
	 * */

	String letraCapital(String str) {

		String cad = "";

		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);

			if ((int) ch >= 65 && (int) ch <= 90) {
				cad += Character.toLowerCase(ch);
				continue;
			} else if ((int) ch >= 97 && (int) ch <= 122) {
				cad += Character.toUpperCase(ch);
				continue;
			} else {
				cad += ch;
			}
		}

		return cad.trim();

	}

	public static void main(String[] args) {
		new SwapCase();
	}

}

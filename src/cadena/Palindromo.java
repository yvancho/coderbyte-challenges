package cadena;

public class Palindromo {

	/**
	 * Using the Java language, have the function Palindrome(str) take the str
	 * parameter being passed and return the string true if the parameter is a
	 * palindrome, (the string is the same forward as it is backward) otherwise
	 * return the string false. For example: "racecar" is also "racecar"
	 * backwards. Punctuation and numbers will not be part of the string.
	 */

	public Palindromo() {
		String cadena = "never odd or even"; 
		System.out.println(palindrome(cadena));
	}

	String palindrome(String str) {

		str = str.replace(" ", "");
		
		String isRight = "false";

		String rev = "";

		for (int i = str.length() - 1; i >= 0; i--) {
			rev += str.charAt(i);
		}

		if (str.equalsIgnoreCase(rev)) {
			isRight = "true";
		}

		return isRight;
	}

	public static void main(String[] args) {
		new Palindromo();
	}

}

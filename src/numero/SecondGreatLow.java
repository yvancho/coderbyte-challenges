package numero;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SecondGreatLow {

	/**
	 * Using the Java language, have the function SecondGreatLow(arr) take the
	 * array of numbers stored in arr and return the second lowest and second
	 * greatest numbers, respectively, separated by a space. For example: if arr
	 * contains [7, 7, 12, 98, 106] the output should be 12 98. The array will
	 * not be empty and will contain at least 2 numbers. It can get tricky if
	 * there's just two numbers!
	 * */

	public SecondGreatLow() {
		Integer[] arr = { 7, 7, 12, 98, 106 };
		System.out.println(segundoGreatLow(arr));
	}

	String segundoGreatLow(Integer[] arr) {
		String cad = "";
		Arrays.sort(arr);
		List<Integer> lista = new ArrayList<>(Arrays.asList(arr));

		// remover repetidos
		for (int i = 0; i < lista.size(); i++) {
			int numBase = lista.get(i);
			for (int j = i; j < lista.size() - 1; j++) {
				if (lista.get(j + 1) == numBase) {
					lista.remove(i);
				}
			}
		}

		int segMenor = lista.get(1);
		int segMayor = lista.get(lista.size() - 2);

		cad += segMenor + " " + segMayor;

		return cad;
	}

	public static void main(String[] args) {
		new SecondGreatLow();
	}

}

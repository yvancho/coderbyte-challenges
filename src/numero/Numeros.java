package numero;

public class Numeros {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Numeros();
	}

	public Numeros() {
		int a[] = { 1, 2, 3, 4, 5 };
		rotar(a, 2);
	}

	public int[] rotar(int a[], int k) {

		k = k % a.length;

		for (int j = 0; j < k; j++) {
			int temp = a[a.length - 1];
			for (int i = a.length - 1; i > 0; i--) {
				a[i] = a[i - 1];
			}
			a[0] = temp;

		}

		return a;
	}
}

package cadena;

public class LetraCapital {

	public LetraCapital() {
		String str = "hola como estas";
		System.out.println(letraCapital(str));
	}

	String letraCapital(String str) {

		String cad = "";
		String[] arr = str.split(" ");

		for (int i = 0; i < arr.length; i++) {
			cad += Character.toUpperCase(arr[i].charAt(0));
			cad += arr[i].substring(1);
			cad += (" ");
		}
		return cad.trim();

	}

	public static void main(String[] args) {
		new LetraCapital();
	}

}

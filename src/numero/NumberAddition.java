package numero;

public class NumberAddition {

	/**
	 * Using the Java language, have the function NumberSearch(str) take the str
	 * parameter, search for all the numbers in the string, add them together,
	 * then return that final number. For example: if str is "88Hello 3World!"
	 * the output should be 91. You will have to differentiate between single
	 * digit numbers and multiple digit numbers like in the example above. So
	 * "55Hello" and "5Hello 5" should return two different answers. Each string
	 * will contain at least one letter or symbol.
	 * */

	public NumberAddition() {
		String str = "75Number9";
		System.out.println(numberSearch(str));
	}

	int numberSearch(String str) {
		int suma = 0;
		String cadTemp = "0";
		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			if (Character.isDigit(ch)) {
				cadTemp += ch;
				continue;
			}
			suma += Integer.valueOf(cadTemp);
			cadTemp = "0";
		}
		suma += Integer.valueOf(cadTemp);
		return suma;
	}

	public static void main(String[] args) {
		new NumberAddition();
	}

}

package numero;

public class MeanMode {

	/**
	 * Using the Java language, have the function MeanMode(arr) take the array
	 * of numbers stored in arr and return 1 if the mode equals the mean, 0 if
	 * they don't equal each other (ie. [5, 3, 3, 3, 1] should return 1 because
	 * the mode (3) equals the mean (3)). The array will not be empty, will only
	 * contain positive integers, and will not contain more than one mode.
	 */

	public MeanMode() {
		int[] arr = { 5, 3, 3, 3, 1 };
		System.out.println(meanMode(arr));
	}

	int meanMode(int[] arr) {

		// mode
		int numMode = arr[0];
		int qMaxRep = 0;
		int temp = 0;
		for (int i = 0; i < arr.length; i++) {
			for (int j = i; j < arr.length; j++) {
				if (arr[i] == arr[j]) {
					temp++;
				}
			}
			if (temp > qMaxRep) {
				qMaxRep = temp;
				numMode = arr[i];
			}
			temp = 0;
		}

		// avg
		int suma = 0;
		for (int i = 0; i < arr.length; i++) {
			suma += arr[i];
		}

		int avg = suma / arr.length;

		if (numMode == avg) {
			return 1;
		}

		return 0;
	}

	public static void main(String[] args) {
		new MeanMode();

	}

}

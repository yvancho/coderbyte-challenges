package cadena;

public class ContarVocales {

	/**
	 * Using the Java language, have the function VowelCount(str) take the str
	 * string parameter being passed and return the number of vowels the string
	 * contains (ie. "All cows eat grass" would return 5). Do not count y as a
	 * vowel for this challenge.
	 */

	public ContarVocales() {
		String cadena = "hola yvancho"; 
		System.out.println(vowelCount(cadena));
	}

	int vowelCount(String str) {

		int q = 0;

		for (int i = 0; i < str.length(); i++) {
			String letra = Character.toString(str.charAt(i));
			if (letra.equals("a") || letra.equals("e") || letra.equals("i")
					|| letra.equals("o") || letra.equals("u")) {
				q++;
			}
		}

		return q;
	}

	public static void main(String[] args) {
		new ContarVocales();
	}

}

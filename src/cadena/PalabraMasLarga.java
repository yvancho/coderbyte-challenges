package cadena;

public class PalabraMasLarga {

	public PalabraMasLarga() {
		String sen = "hola mundo absalucion absolution ebsolucion aaaaaaaaaaaaaaaaaaaaaaaaa";
		System.out.println(longestWord(sen));
	}

	String longestWord(String sen) {

		String newSen = "";

		// clean special characters
		for (int i = 0; i < sen.length(); i++) {
			char c = sen.charAt(i);
			if ((int) c > 64 && (int) c < 91 || (int) c > 96 && (int) c < 123
					|| (int) c == 32) {
				newSen += c;
			}
		}

		// pass references and split each word.
		sen = newSen;
		String[] arr = sen.split(" ");

		// Also, preparing to compare the longest word
		int longest = arr[0].length();
		String longestCad = arr[0];

		// Compare the longest
		for (int i = 0; i < arr.length; i++) {
			int wordEva = arr[i].length();
			if (wordEva > longest && longest != wordEva) {
				longestCad = arr[i];
				longest = arr[i].length();
			}

		}

		return longestCad;

	}

	public static void main(String[] args) {
		new PalabraMasLarga();

	}

}

package cadena;

public class DashInsert {

	/**
	 * Using the Java language, have the function DashInsert(str) insert dashes
	 * ('-') between each two odd numbers in str. For example: if str is 454793
	 * the output should be 4547-9-3. Don't count zero as an odd number.
	 */

	public DashInsert() {
		String cadena = "450354793";
		System.out.println(dashInsert(cadena));
	}

	String dashInsert(String str) {

		String retorno = "";
		char[] arrChar = str.toCharArray();
		int[] arrInt = new int[str.length()];

		for (int i = 0; i < arrChar.length; i++) {
			int num = Integer.valueOf(Character.toString(arrChar[i]));
			arrInt[i] = num;
		}

		for (int i = 0; i < arrInt.length - 1; i++) {
			if (arrInt[i] % 2 != 0 && arrInt[i] != 0) {
				if (arrInt[i - 1] % 2 != 0 && arrInt[i + 1] % 2 != 0) {
					retorno += "-" + arrInt[i] + "-";
					continue;
				}
			}
			retorno += arrInt[i];
		}
		retorno += arrInt[arrInt.length - 1];
		return retorno;
	}

	public static void main(String[] args) {
		new DashInsert();
	}

}

package cadena;

public class SwapII {

	public SwapII() {
		String str = "6Hello4 -8World, 7 yes3"; // hELLO wORLD
		System.out.println(swapII(str));
	}

	/**
	 * Using the Java language, have the function SwapII(str) take the str
	 * parameter and swap the case of each character. Then, if a letter is
	 * between two numbers (without separation), switch the places of the two
	 * numbers. For example: if str is "6Hello4 -8World, 7 yes3" the output
	 * should be 4hELLO6 -8wORLD, 7 YES3.
	 * */

	String swapII(String str) {
		String swapLetters = swapLetters(str);
		
		int primNum = -1;
		int segNum = -2;
		
		for (int i = 0; i < swapLetters.length(); i++) {
			
			char ch = str.charAt(i);
			if(Character.isDigit(ch)){
				primNum = Integer.valueOf(Character.toString(ch));
			}
			
		}
		
		return null;
	}

	private String swapLetters(String str) {
		String cad = "";
		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);

			if ((int) ch >= 65 && (int) ch <= 90) {
				cad += Character.toLowerCase(ch);
				continue;
			} else if ((int) ch >= 97 && (int) ch <= 122) {
				cad += Character.toUpperCase(ch);
				continue;
			} else {
				cad += ch;
			}
		}

		return cad.trim();
	}

	public static void main(String[] args) {
		new SwapII();
	}

}

package cadena;

public class SimbolosSimples {

	public SimbolosSimples() {
		String str = "+d+";
		System.out.println(simpleSimbols(str));
	}

	String simpleSimbols(String str) {

		boolean isRight = false;

		// begins and ends with "+"
		if (str.startsWith("+") && str.endsWith("+")) {
			isRight = true;
		} else {
			return "false";
		}

		// contains "="
		if (!str.contains("=")) {
			return "false";
		}

		// at least one letter
		for (int i = 0; i < str.length(); i++) {
			if (!Character.isLetter(str.charAt(i))) {
				isRight = false;
			} else {
				isRight = true;
				break;
			}
		}

		// letter surrounded by "+" and "+"
		for (int i = 1; i < str.length() - 1; i++) {
			if (Character.isLetter(str.charAt(i))) {
				if (!Character.toString(str.charAt(i - 1)).equals("+")
						|| !Character.toString(str.charAt(i + 1)).equals("+")) {
					isRight = false;
					break;
				} else {
					isRight = true;
				}
			}
		}

		return String.valueOf(isRight);

	}

	public static void main(String[] args) {
		new SimbolosSimples();
	}

}

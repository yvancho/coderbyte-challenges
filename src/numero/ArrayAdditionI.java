package numero;

import java.util.Arrays;

public class ArrayAdditionI {

	/**
	 * Using the Java language, have the function ArrayAdditionI(arr) take the
	 * array of numbers stored in arr and return the string true if any
	 * combination of numbers in the array can be added up to equal the largest
	 * number in the array, otherwise return the string false. For example: if
	 * arr contains [4, 6, 23, 10, 1, 3] the output should return true because 4
	 * + 6 + 10 + 3 = 23. The array will not be empty, will not contain all the
	 * same elements, and may contain negative numbers.
	 */

	public ArrayAdditionI() {
		int[] arr1 = { 4, 6, 23, 10, 1, 3 }; // 1, 3, 4, 6, 10, 23

		System.out.println(arrayAdd(arr1));
	}

	String arrayAdd(int[] arr) {

		// sort
		Arrays.sort(arr);	

		// search for combinations
		int suma = 0;
		
		int k = arr[0];
		for (int i = 0; i < arr.length - 1; i++) {
			suma = k+arr[i];		
			System.out.println(suma);
		
		}


		return "false";

	}

	public static void main(String[] args) {
		new ArrayAdditionI();
	}

}

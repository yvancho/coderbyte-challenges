package numero;

import java.text.NumberFormat;
import java.util.Locale;

public class FormattedDivision {

	/**
	 * Using the Java language, have the function FormattedDivision(num1,num2)
	 * take both parameters being passed, divide num1 by num2, and return the
	 * result as a string with properly formatted commas and 4 significant
	 * digits after the decimal place. For example: if num1 is 123456789 and
	 * num2 is 10000 the output should be "12,345.6789". The output must contain
	 * a number in the one's place even if it is a zero.
	 */

	public FormattedDivision() {
		int num1 = 2;
		int num2 = 3;
		System.out.println(formattedDivision(num1, num2));
	}

	String formattedDivision(int num1, int num2) {

		String numFormat = "";
		Double numero = Double.valueOf(num1) / Double.valueOf(num2);

		numFormat = NumberFormat.getNumberInstance(Locale.getDefault()).format(
				numero);

		return numFormat;

	}

	public static void main(String[] args) {
		new FormattedDivision();
	}

}

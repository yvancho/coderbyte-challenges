package numero;

public class CuentaMinutos {

	/**
	 * Using the Java language, have the function CountingMinutesI(str) take the
	 * str parameter being passed which will be two times (each properly
	 * formatted with a colon and am or pm) separated by a hyphen and return the
	 * total number of minutes between the two times. The time will be in a 12
	 * hour clock format. For example: if str is 9:00am-10:00am then the output
	 * should be 60. If str is 1:00pm-11:00am the output should be 1320.
	 * */

	public CuentaMinutos() {
		String str = "1:23am-1:08am";
		System.out.println(countingMinutesI(str));
	}

	String countingMinutesI(String str) {

		// split and get the 2 times
		String[] arrHoras = str.split("-");

		// obtener los 2 tiempos
		String primTiempo = arrHoras[0];
		String segTiempo = arrHoras[1];

		// obtener las horas
		int primHora = Integer.valueOf(primTiempo.substring(0, primTiempo.indexOf(":")));
		int segHora = Integer.valueOf(segTiempo.substring(0, segTiempo.indexOf(":")));

		// obtener los minutos
		int primMinutos = Integer.valueOf(primTiempo.substring(
				primTiempo.indexOf(":") + 1, primTiempo.length() - 2));
		int segMinutos = Integer.valueOf(segTiempo.substring(
				segTiempo.indexOf(":") + 1, segTiempo.length() - 2));

		// los minutos totales horas + minutos
		primMinutos += (primHora * 60);
		segMinutos += (segHora * 60);

		// si es pm, sumamos: 60*12 = 720 minutos
		if (primTiempo.contains("pm") && primHora > 12) {
			primMinutos += 720;
		}

		if (segTiempo.contains("pm") && segHora > 12) {
			segHora += 720;
		}

		// si los minutos del segundo tiempo son menores que los priemros,
		// sumamos 24 horas: 24*60=1440
		if (segMinutos < primMinutos) {
			segMinutos += (24 * 60);
		}

		int resultado = segMinutos - primMinutos;
		str = resultado + "";

		return str;

	}

	public static void main(String[] args) {
		new CuentaMinutos();
	}

}
